import tinycolor from 'tinycolor2'

import { sequentialGroup } from './utils'
import * as consts from './consts'
import { drawCircle } from './canvas'


export function renderLine(ctx, line, color) {
  const lineGroups = sequentialGroup(line, (o) => o.size)
  const style = tinycolor(color).toHexString()

  for (let i = 0; i < lineGroups.length; i++) {
    const lineGroup = lineGroups[i]

    ctx.fillStyle = style
    ctx.strokeStyle = style
    ctx.lineWidth = lineGroup[0].size
    ctx.lineCap = 'round'
    ctx.lineJoin = 'round'

    ctx.beginPath()
    drawCircle(ctx, lineGroup[0].position, lineGroup[0].size / 2)
    ctx.fill()
    ctx.closePath()

    ctx.beginPath()
    ctx.moveTo(lineGroup[0].position[0], lineGroup[0].position[1])
    for (let j = 1; j < lineGroup.length; j++) {
      ctx.lineTo(
        lineGroup[j].position[0],
        lineGroup[j].position[1])
    }

    if (i < lineGroups.length - 1) {
      ctx.lineTo(
        lineGroups[i + 1][0].position[0],
        lineGroups[i + 1][0].position[1]
      )
    }

    ctx.stroke()
    ctx.closePath()
  }
}


export function renderPlayerLinePreview(ctx, player) {
  renderLine(ctx, player.line, player.color)
}


export function renderPlayerLineOnTiles(tileSet, player) {
  renderLineOnTileSet(tileSet, player.line, player.color, player.size)
}


export function renderLineOnTileSet(tileSet, line, color) {
  const tilesToPaint = getLineTilePositions(line)
    .map((pos) => tileSet.getTile(pos))

  for (const tile of tilesToPaint) {
    tile.renderOn((ctx) => {
      renderLine(ctx, line, color)
    })
  }
}

export function getLineTilePositions(line) {
  const tilePosToPaint = []
  const _add = (x) => {
    if (!tilePosToPaint.find(
        (p) => p[0] === x[0] && p[1] === x[1]))
      tilePosToPaint.push(x)
    }

  _getLineTilePositions(line, [-1, -1]).map(_add)
  _getLineTilePositions(line, [ 1, -1]).map(_add)
  _getLineTilePositions(line, [ 1,  1]).map(_add)
  _getLineTilePositions(line, [-1,  1]).map(_add)

  return tilePosToPaint
}


function _getLineTilePositions(line, offset) {
  const tilePosToPaint = []

  tilePosToPaint.push([
    Math.round(
        (line[0].position[0] + (line[0].size * offset[0])) / consts.TILE_SIZE),
    Math.round(
        (line[0].position[1] + (line[0].size * offset[1])) / consts.TILE_SIZE)
  ])

  for (let i = 0; i < line.length - 1; i++) {
    const point = line[i]
    const pointNext = line[i + 1]

    const pos = point.position
    const posNext = pointNext.position
    const size = point.size > pointNext.size ? point.size : pointNext.size

    const pbBox = {
      top: (pos[1] < posNext[1] ? pos[1] : posNext[1]) - size,
      bottom: (pos[1] > posNext[1] ? pos[1] : posNext[1]) + size,
      left: (pos[0] < posNext[0] ? pos[0] : posNext[0]) - size,
      right: (pos[0] > posNext[0] ? pos[0] : posNext[0]) + size
    }

    const tilePos = [
      Math.round((pos[0] + (size * offset[0])) / consts.TILE_SIZE),
      Math.round((pos[1] + (size * offset[1])) / consts.TILE_SIZE)
    ]

    const nextTilePos = [
      Math.round((posNext[0] + (size * offset[0])) / consts.TILE_SIZE),
      Math.round((posNext[1] + (size * offset[1])) / consts.TILE_SIZE)
    ]

    const diff = [
      nextTilePos[0] - tilePos[0],
      nextTilePos[1] - tilePos[1]
    ]

    const centreTile = [
      tilePos[0] * consts.TILE_SIZE,
      tilePos[1] * consts.TILE_SIZE
    ]

    for (let x = 0; x <= Math.abs(diff[0]); x++) {
      for (let y = 0; y <= Math.abs(diff[1]); y++) {
        const tbBox = {
          top: centreTile[1] - (consts.TILE_SIZE / 2) +
            (y * consts.TILE_SIZE * Math.sign(diff[1])),
          bottom: centreTile[1] + (consts.TILE_SIZE / 2) +
            (y * consts.TILE_SIZE * Math.sign(diff[1])),
          left: centreTile[0] - (consts.TILE_SIZE / 2) +
            (x * consts.TILE_SIZE * Math.sign(diff[0])),
          right: centreTile[0] + (consts.TILE_SIZE / 2) +
            (x * consts.TILE_SIZE * Math.sign(diff[0]))
        }

        const thisTilePos = [
          tilePos[0] + (x * Math.sign(diff[0])),
          tilePos[1] + (y * Math.sign(diff[1]))
        ]

        if (_intersect(pbBox, tbBox))
          if (!tilePosToPaint.find(
              (p) => p[0] === thisTilePos[0] && p[1] === thisTilePos[1]))
            tilePosToPaint.push(thisTilePos)
      }
    }
  }

  return tilePosToPaint
}

function _intersect(b1, b2) {
  return !(
    b2.left > b1.right ||
    b2.right < b1.left ||
    b2.top > b1.bottom ||
    b2.bottom < b1.top)
}
