export class Map2D {
  constructor() {
    this._map = {}
  }

  get([x, y]) {
    if (x in this._map && y in this._map[x])
      return this._map[x][y]

    return undefined
  }

  set([x, y], value) {
    if (!(x in this._map))
      this._map[x] = []

    this._map[x][y] = value
  }

  keys() {
    return Array.from(iterate2D(this._map, { keys: true }))
  }

  values() {
    return Array.from(iterate2D(this._map, { values: true }))
  }

  items() {
    return Array.from(iterate2D(this._map))
  }

  [Symbol.iterator]() {
    return this.items()
  }
}

export function* iterate2D(map, opts = { keys: true, values: true }) {
  for (const x in map) {
    for (const y in map[x]) {
      if (opts.keys && opts.values)
        yield {
          key: [parseInt(x), parseInt(y)],
          value: map[x][y]
        }

      if (opts.keys)
        yield [parseInt(x), parseInt(y)]

      if (opts.values)
        yield map[x][y]
    }
  }
}

export function sequentialGroup(collection, keyFunc) {
  const groups = []
  let lastKey = null
  let currentGroup = null

  for (let i = 0; i < collection.length; i++) {
    const obj = collection[i]
    const key = keyFunc(obj)

    if (key != lastKey) {
      currentGroup = []
      groups.push(currentGroup)
    }

    currentGroup.push(obj)
    lastKey = key
  }

  return groups
}
