export function drawCircle(ctx, position, radius) {
  ctx.arc(
    position[0], position[1],
    radius,
    0, 2 * Math.PI)
}
