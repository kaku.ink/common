import * as _ from 'lodash';
import * as consts from './consts'
import { Map2D } from './utils'


class Tile {
  constructor(ctxCreator, position) {
    this.ctx = ctxCreator(consts.TILE_SIZE, consts.TILE_SIZE)
    this.position = position
    this.dirty = false

    this.ctx.fillStyle = 'white'
    this.ctx.fillRect(0, 0, consts.TILE_SIZE, consts.TILE_SIZE)
    this.ctx.setTransform(
      1,
      0,
      0,
      1,
      (-this.position[0] * consts.TILE_SIZE) + (consts.TILE_SIZE / 2),
      (-this.position[1] * consts.TILE_SIZE) + (consts.TILE_SIZE / 2))
  }

  renderOn(renderFunc) {
    renderFunc(this.ctx)
    this.dirty = true
  }

  init(image) {
    this.ctx.drawImage(image,
      (this.position[0] * consts.TILE_SIZE) - (consts.TILE_SIZE / 2),
      (this.position[1] * consts.TILE_SIZE) - (consts.TILE_SIZE / 2))
  }
}


class TileSet {
  constructor(ctxCreator) {
    if (!ctxCreator)
      throw 'No canvas creator provided.'

    this.tiles = new Map2D()
    this._ctxCreator = ctxCreator
  }


  initTile(position, image) {
    this.getTile(position).init(image)
  }

  renderOnTile(position, renderFunc) {
    const tilePosition = [
      Math.round(position[0] / consts.TILE_SIZE),
      Math.round(position[1] / consts.TILE_SIZE)
    ]

    this.getTile(tilePosition).renderOn(renderFunc)
  }

  findTilesInViewRange(viewSize, pan, scale) {
    const tileViewSize = [
      Math.ceil(viewSize[0] / scale / consts.TILE_SIZE) *
        consts.TILE_SIZE,
      Math.ceil(viewSize[1] / scale / consts.TILE_SIZE) *
        consts.TILE_SIZE
    ]

    return this.tiles.values().filter(
      (tile) => {
        const xCheck = (offset) => _.inRange(
          tile.position[0] * consts.TILE_SIZE + offset,
          pan[0] - (tileViewSize[0] / 2),
          pan[0] + (tileViewSize[0] / 2))

        const yCheck = (offset) => _.inRange(
          tile.position[1] * consts.TILE_SIZE + offset,
          pan[1] - (tileViewSize[1] / 2),
          pan[1] + (tileViewSize[1] / 2))

        const boundCheck = (xOffset, yOffset) =>
          xCheck(xOffset) && yCheck(yOffset)

        return (
          boundCheck( consts.TILE_SIZE / 2,  consts.TILE_SIZE / 2) ||
          boundCheck( consts.TILE_SIZE / 2, -consts.TILE_SIZE / 2) ||
          boundCheck(-consts.TILE_SIZE / 2,  consts.TILE_SIZE / 2) ||
          boundCheck(-consts.TILE_SIZE / 2, -consts.TILE_SIZE / 2)
        )
      })
  }

  getTile(position) {
    if (!this.tiles.get(position)) {
      let tile = new Tile(this._ctxCreator, position)
      this.tiles.set(position, tile)
    }

    return this.tiles.get(position)
  }
}


export {
  Tile,
  TileSet
}
